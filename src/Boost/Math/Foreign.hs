{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE MagicHash           #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UnboxedTuples       #-}

module Boost.Math.Foreign where

import           Control.Arrow            (first)
import           Control.Exception        (Exception, finally, throw)
import           Control.Monad            (forM_)
import qualified Data.Vector              as V
import qualified Data.Vector.Generic      as GV
import qualified Data.Vector.Storable     as SV
import           Foreign                  (free, mallocBytes)
import           Foreign.Marshal.Array    (advancePtr)
import           Foreign.Storable         (Storable)
import           GHC.Exts                 (Int (..), Ptr (..))
import           GHC.Prim                 (copyByteArrayToAddr#,
                                           sizeofByteArray#)
import           GHC.Types                (IO (..))
import           Numeric.MPFR.Types
import           Numeric.Rounded
import           Numeric.Rounded.Internal (Rounded (..))

data MPFRPrecisionError = MPFRPrecisionError
  deriving (Eq, Ord, Show)

instance Exception MPFRPrecisionError

withOutRounded' :: Precision p => (Ptr MPFR -> IO a) -> IO (Rounded r p, a)
withOutRounded' = fmap (first (maybe (throw MPFRPrecisionError) id)) . withOutRounded

mapArrayM_ :: Storable a => Int -> (Ptr a -> IO ()) -> Ptr a -> IO ()
mapArrayM_ len f ptr =
  forM_ [0 .. len-1] $ \i -> f (advancePtr ptr i)

withInRoundedVec
  :: V.Vector (Rounded r p)
  -> (Ptr MPFR -> IO a)
  -> IO a
withInRoundedVec xs f = do
  mpfrs <- fmap GV.convert (mapM mallocRounded xs)
  SV.unsafeWith mpfrs f
    `finally` SV.mapM_ (free . mpfrD) mpfrs

mallocRounded :: Rounded r p -> IO MPFR
mallocRounded (Rounded prec sgn e ba#) = do
  let bytes = I# (sizeofByteArray# ba#)
  ptr@(Ptr addr#) <- mallocBytes bytes
  IO (\s -> (# copyByteArrayToAddr# ba# 0# addr# (sizeofByteArray# ba#) s, () #))
  return MPFR
    { mpfrPrec = prec
    , mpfrSign = sgn
    , mpfrExp = e
    , mpfrD = ptr
    }
