{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}
{-# LANGUAGE DataKinds                #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE MultiWayIf               #-}
{-# LANGUAGE PatternSynonyms          #-}
{-# LANGUAGE PolyKinds                #-}
{-# LANGUAGE ScopedTypeVariables      #-}

module Boost.Math.SpecialFunctions
  ( tgamma
  , gamma
  , tgamma1pm1
  , lgamma
  , log_gamma
  , digamma
  , trigamma
  , polygamma
  , tgamma_ratio
  , gamma_p
  , gamma_q
  , tgamma_lower
  , tgamma_inc
  , gamma_q_inv
  , gamma_p_inv
  , gamma_q_inva
  , gamma_p_inva
  , gamma_p_derivative
  , beta
  , ibeta
  , ibetac
  , beta_inc
  , betac_inc
  , ibeta_inv
  , ibetac_inv
  , ibeta_inva
  , ibetac_inva
  , ibeta_invb
  , ibetac_invb
  , ibeta_derivative
  , erf
  , erfc
  , erf_inv
  , erfc_inv
  , cyl_bessel_j
  , cyl_neumann
  , cyl_bessel_i
  , cyl_bessel_k
  , airy_ai
  , airy_bi
  , airy_ai_prime
  , airy_bi_prime
  , ellint_1
  , ellint_2
  , ellint_d
  , ellint_1_inc
  , ellint_2_inc
  , ellint_3
  , ellint_d_inc
  , jacobi_zeta
  , heuman_lambda
  , ellint_3_inc
  , zeta
  , expint_ei
  , expint_en
  , hypergeometric_1F0
  , hypergeometric_0F1
  , hypergeometric_2F0
  , hypergeometric_1F1
  , hypergeometric_1F1_regularized
  , log_hypergeometric_1F1
  , hypergeometric_pFq
  , hypergeometric_2F1
  ) where

import           Boost.Math.Foreign (withInRoundedVec, withOutRounded')
import           Control.Exception  (Exception, throw)
import           Data.Proxy         (Proxy (..))
import           Data.Vector        (Vector)
import qualified Data.Vector        as V
import           Foreign.C.Error    (Errno (..), eDOM, eOK, eRANGE)
import           Foreign.C.Types    (CInt (..), CUInt (..))
import           GHC.Exts           (Ptr)
import           Numeric.MPFR.Types (MPFR, MPFRPrec (..))
import           Numeric.Rounded    (Precision, Rounded, Rounding, precision,
                                     withInRounded)
import           System.IO.Unsafe   (unsafePerformIO)

data BoostMathError
  = DomainError String
  | RangeError String
  | PrecisionError String
  | UnknownError CInt String
  deriving (Eq, Ord, Show)

instance Exception BoostMathError

defaultCheckInfo :: String -> a -> Errno -> a
defaultCheckInfo functionName a e@(Errno e') = if
  | e == eOK    -> a
  | e == eDOM   -> throw (DomainError functionName)
  | e == eRANGE -> throw (RangeError functionName)
  | otherwise   -> throw (UnknownError e' functionName)

type Unary   = Ptr MPFR -> Ptr MPFR -> MPFRPrec -> IO Errno
type Binary  = Ptr MPFR -> Unary
type Ternary = Ptr MPFR -> Binary

type H_Unary r p = Rounded r p -> Rounded r p
type H_Binary r p = Rounded r p -> H_Unary r p
type H_Ternary r p = Rounded r p -> H_Binary r p

unary :: forall r p . (Rounding r, Precision p) => String -> Unary -> H_Unary r p
unary functionName go x = unpack $ unsafePerformIO $
  withInRounded x $ \xPtr ->
  withOutRounded' $ \yPtr ->
  go xPtr yPtr prec
  where
    prec = fromIntegral (precision (Proxy :: Proxy p))
    unpack (res, info) = defaultCheckInfo functionName res info

binary :: forall r p . (Rounding r, Precision p) => String -> Binary -> H_Binary r p
binary functionName go x1 x2 = unpack $ unsafePerformIO $
  withInRounded x1 $ \x1Ptr ->
  withInRounded x2 $ \x2Ptr ->
  withOutRounded' $ \yPtr ->
  go x1Ptr x2Ptr yPtr prec
  where
    prec = fromIntegral (precision (Proxy :: Proxy p))
    unpack (res, info) = defaultCheckInfo functionName res info


ternary :: forall r p . (Rounding r, Precision p) => String -> Ternary -> H_Ternary r p
ternary functionName go x1 x2 x3 = unpack $ unsafePerformIO $
  withInRounded x1 $ \x1Ptr ->
  withInRounded x2 $ \x2Ptr ->
  withInRounded x3 $ \x3Ptr ->
  withOutRounded' $ \yPtr ->
  go x1Ptr x2Ptr x3Ptr yPtr prec
  where
    prec = fromIntegral (precision (Proxy :: Proxy p))
    unpack (res, info) = defaultCheckInfo functionName res info

-- boost/math/special_functions/gamma.hpp
foreign import ccall "boost_math_tgamma"             boost_math_tgamma             :: Unary
foreign import ccall "boost_math_tgamma1pm1"         boost_math_tgamma1pm1         :: Unary
foreign import ccall "boost_math_lgamma"             boost_math_lgamma             :: Unary
foreign import ccall "boost_math_digamma"            boost_math_digamma            :: Unary
foreign import ccall "boost_math_trigamma"           boost_math_trigamma           :: Unary
foreign import ccall "boost_math_polygamma"          boost_math_polygamma          :: CInt -> Unary
foreign import ccall "boost_math_tgamma_ratio"       boost_math_tgamma_ratio       :: Binary
foreign import ccall "boost_math_gamma_p"            boost_math_gamma_p            :: Binary
foreign import ccall "boost_math_gamma_q"            boost_math_gamma_q            :: Binary
foreign import ccall "boost_math_tgamma_lower"       boost_math_tgamma_lower       :: Binary
foreign import ccall "boost_math_tgamma_inc"         boost_math_tgamma_inc         :: Binary
foreign import ccall "boost_math_gamma_q_inv"        boost_math_gamma_q_inv        :: Binary
foreign import ccall "boost_math_gamma_p_inv"        boost_math_gamma_p_inv        :: Binary
foreign import ccall "boost_math_gamma_q_inva"       boost_math_gamma_q_inva       :: Binary
foreign import ccall "boost_math_gamma_p_inva"       boost_math_gamma_p_inva       :: Binary
foreign import ccall "boost_math_gamma_p_derivative" boost_math_gamma_p_derivative :: Binary

tgamma, gamma, tgamma1pm1, lgamma, log_gamma, digamma, trigamma
  :: (Rounding r, Precision p) => H_Unary r p
tgamma     = unary "tgamma"     boost_math_tgamma
gamma      = tgamma
tgamma1pm1 = unary "tgamma1pm1" boost_math_tgamma1pm1
lgamma     = unary "lgamma"     boost_math_lgamma
log_gamma  = lgamma
digamma    = unary "digamma"    boost_math_digamma
trigamma   = unary "trigamma"   boost_math_trigamma

polygamma :: (Rounding r, Precision p) => Int -> H_Unary r p
polygamma n = unary "polygamma" (boost_math_polygamma (fromIntegral n))

tgamma_ratio, gamma_p, gamma_q, tgamma_lower, tgamma_inc, gamma_q_inv, gamma_p_inv, gamma_q_inva, gamma_p_inva, gamma_p_derivative
  :: (Rounding r, Precision p) => H_Binary r p
tgamma_ratio       = binary "tgamma_ratio"       boost_math_tgamma_ratio
gamma_p            = binary "gamma_p"            boost_math_gamma_p
gamma_q            = binary "gamma_q"            boost_math_gamma_q
tgamma_lower       = binary "tgamma_lower"       boost_math_tgamma_lower
tgamma_inc         = binary "tgamma_inc"         boost_math_tgamma_inc
gamma_q_inv        = binary "gamma_q_inv"        boost_math_gamma_q_inv
gamma_p_inv        = binary "gamma_p_inv"        boost_math_gamma_p_inv
gamma_q_inva       = binary "gamma_q_inva"       boost_math_gamma_q_inva
gamma_p_inva       = binary "gamma_p_inva"       boost_math_gamma_p_inva
gamma_p_derivative = binary "gamma_p_derivative" boost_math_gamma_p_derivative

-- boost/math/special_functions/beta.hpp
foreign import ccall "boost_math_beta"             boost_math_beta             :: Binary
foreign import ccall "boost_math_ibeta"            boost_math_ibeta            :: Ternary
foreign import ccall "boost_math_ibetac"           boost_math_ibetac           :: Ternary
foreign import ccall "boost_math_beta_inc"         boost_math_beta_inc         :: Ternary
foreign import ccall "boost_math_betac_inc"        boost_math_betac_inc        :: Ternary
foreign import ccall "boost_math_ibeta_inv"        boost_math_ibeta_inv        :: Ternary
foreign import ccall "boost_math_ibetac_inv"       boost_math_ibetac_inv       :: Ternary
foreign import ccall "boost_math_ibeta_inva"       boost_math_ibeta_inva       :: Ternary
foreign import ccall "boost_math_ibetac_inva"      boost_math_ibetac_inva      :: Ternary
foreign import ccall "boost_math_ibeta_invb"       boost_math_ibeta_invb       :: Ternary
foreign import ccall "boost_math_ibetac_invb"      boost_math_ibetac_invb      :: Ternary
foreign import ccall "boost_math_ibeta_derivative" boost_math_ibeta_derivative :: Ternary

beta :: (Rounding r, Precision p) => H_Binary r p
beta = binary "beta" boost_math_beta

ibeta, ibetac, beta_inc, betac_inc, ibeta_inv, ibetac_inv, ibeta_inva, ibetac_inva, ibeta_invb, ibetac_invb, ibeta_derivative
  :: (Rounding r, Precision p) => H_Ternary r p
ibeta            = ternary "ibeta"       boost_math_ibeta
ibetac           = ternary "ibetac"      boost_math_ibetac
beta_inc         = ternary "beta_inc"    boost_math_beta_inc
betac_inc        = ternary "betac_inc"   boost_math_betac_inc
ibeta_inv        = ternary "ibeta_inv"   boost_math_ibeta_inv
ibetac_inv       = ternary "ibetac_inv"  boost_math_ibetac_inv
ibeta_inva       = ternary "ibeta_inva"  boost_math_ibeta_inva
ibetac_inva      = ternary "ibetac_inva" boost_math_ibetac_inva
ibeta_invb       = ternary "ibeta_invb"  boost_math_ibeta_invb
ibetac_invb      = ternary "ibetac_invb" boost_math_ibetac_invb
ibeta_derivative = ternary "ibetac_invb" boost_math_ibeta_derivative

-- boost/math/special_functions/erf.hpp
foreign import ccall "boost_math_erf"      boost_math_erf      :: Unary
foreign import ccall "boost_math_erfc"     boost_math_erfc     :: Unary
foreign import ccall "boost_math_erf_inv"  boost_math_erf_inv  :: Unary
foreign import ccall "boost_math_erfc_inv" boost_math_erfc_inv :: Unary

erf, erfc, erf_inv, erfc_inv
  :: (Rounding r, Precision p) => H_Unary r p
erf      = unary "erf"      boost_math_erf
erfc     = unary "erfc"     boost_math_erfc
erf_inv  = unary "erf_inv"  boost_math_erf_inv
erfc_inv = unary "erfc_inv" boost_math_erfc_inv

-- boost/math/special_functions/bessel.hpp
foreign import ccall "boost_math_cyl_bessel_j" boost_math_cyl_bessel_j :: Binary
foreign import ccall "boost_math_cyl_neumann"  boost_math_cyl_neumann  :: Binary
foreign import ccall "boost_math_cyl_bessel_i" boost_math_cyl_bessel_i :: Binary
foreign import ccall "boost_math_cyl_bessel_k" boost_math_cyl_bessel_k :: Binary

cyl_bessel_j, cyl_neumann, cyl_bessel_i, cyl_bessel_k
  :: (Rounding r, Precision p) => H_Binary r p
cyl_bessel_j = binary "cyl_bessel_j" boost_math_cyl_bessel_j
cyl_neumann  = binary "cyl_neumann"  boost_math_cyl_neumann
cyl_bessel_i = binary "cyl_bessel_i" boost_math_cyl_bessel_i
cyl_bessel_k = binary "cyl_bessel_k" boost_math_cyl_bessel_k

-- boost/math/special_functions/airy.hpp: airy_ai, airy_bi, airy_ai_prime, airy_bi_prime
foreign import ccall "boost_math_airy_ai"       boost_math_airy_ai       :: Unary
foreign import ccall "boost_math_airy_bi"       boost_math_airy_bi       :: Unary
foreign import ccall "boost_math_airy_ai_prime" boost_math_airy_ai_prime :: Unary
foreign import ccall "boost_math_airy_bi_prime" boost_math_airy_bi_prime :: Unary

airy_ai, airy_bi, airy_ai_prime, airy_bi_prime
  :: (Rounding r, Precision p) => H_Unary r p
airy_ai       = unary "airy_ai"       boost_math_airy_ai
airy_bi       = unary "airy_bi"       boost_math_airy_bi
airy_ai_prime = unary "airy_ai_prime" boost_math_airy_ai_prime
airy_bi_prime = unary "airy_bi_prime" boost_math_airy_bi_prime

-- elliptic integrals

foreign import ccall "boost_math_ellint_1" boost_math_ellint_1 :: Unary
foreign import ccall "boost_math_ellint_2" boost_math_ellint_2 :: Unary
foreign import ccall "boost_math_ellint_d" boost_math_ellint_d :: Unary

ellint_1, ellint_2, ellint_d
  :: (Rounding r, Precision p) => H_Unary r p
ellint_1 = unary "ellint_1" boost_math_ellint_1
ellint_2 = unary "ellint_2" boost_math_ellint_2
ellint_d = unary "ellint_d" boost_math_ellint_d

foreign import ccall "boost_math_ellint_1_inc"  boost_math_ellint_1_inc  :: Binary
foreign import ccall "boost_math_ellint_2_inc"  boost_math_ellint_2_inc  :: Binary
foreign import ccall "boost_math_ellint_3"      boost_math_ellint_3      :: Binary
foreign import ccall "boost_math_ellint_d_inc"  boost_math_ellint_d_inc  :: Binary
foreign import ccall "boost_math_jacobi_zeta"   boost_math_jacobi_zeta   :: Binary
foreign import ccall "boost_math_heuman_lambda" boost_math_heuman_lambda :: Binary

ellint_1_inc, ellint_2_inc, ellint_3, ellint_d_inc, jacobi_zeta, heuman_lambda
  :: (Rounding r, Precision p) => H_Binary r p
ellint_1_inc  = binary "ellint_1_inc"  boost_math_ellint_1_inc
ellint_2_inc  = binary "ellint_2_inc"  boost_math_ellint_2_inc
ellint_3      = binary "ellint_3"      boost_math_ellint_3
ellint_d_inc  = binary "ellint_d_inc"  boost_math_ellint_d_inc
jacobi_zeta   = binary "jacobi_zeta"   boost_math_jacobi_zeta
heuman_lambda = binary "heuman_lambda" boost_math_heuman_lambda

foreign import ccall "boost_math_ellint_3_inc" boost_math_ellint_3_inc :: Ternary

ellint_3_inc :: (Rounding r, Precision p) => H_Ternary r p
ellint_3_inc = ternary "ellint_3_inc" boost_math_ellint_3_inc

-- boost/math/special_functions/zeta.hpp

foreign import ccall "boost_math_zeta" boost_math_zeta :: Unary

zeta :: (Rounding r, Precision p) => H_Unary r p
zeta = unary "zeta" boost_math_zeta

-- boost/math/special_functions/expint.hpp

foreign import ccall "boost_math_expint_ei" boost_math_expint_ei :: Unary
foreign import ccall "boost_math_expint_en" boost_math_expint_en :: CUInt -> Unary

expint_ei :: (Rounding r, Precision p) => H_Unary r p
expint_ei = unary "expint_ei" boost_math_expint_ei

expint_en :: (Rounding r, Precision p) => Word -> H_Unary r p
expint_en n = unary "expint_en" (boost_math_expint_en (fromIntegral n))

-- hypergeometric functions

foreign import ccall "boost_math_hypergeometric_1F0" boost_math_hypergeometric_1F0 :: Binary
foreign import ccall "boost_math_hypergeometric_0F1" boost_math_hypergeometric_0F1 :: Binary

hypergeometric_1F0, hypergeometric_0F1
  :: (Rounding r, Precision p) => H_Binary r p
hypergeometric_1F0  = binary "hypergeometric_1F0" boost_math_hypergeometric_1F0
hypergeometric_0F1  = binary "hypergeometric_0F1" boost_math_hypergeometric_0F1

foreign import ccall "boost_math_hypergeometric_2F0"             boost_math_hypergeometric_2F0             :: Ternary
foreign import ccall "boost_math_hypergeometric_1F1"             boost_math_hypergeometric_1F1             :: Ternary
foreign import ccall "boost_math_hypergeometric_1F1_regularized" boost_math_hypergeometric_1F1_regularized :: Ternary
foreign import ccall "boost_math_log_hypergeometric_1F1"         boost_math_log_hypergeometric_1F1         :: Ternary

hypergeometric_2F0, hypergeometric_1F1, hypergeometric_1F1_regularized, log_hypergeometric_1F1
  :: (Rounding r, Precision p) => H_Ternary r p
hypergeometric_2F0             = ternary "hypergeometric_2F0"             boost_math_hypergeometric_2F0
hypergeometric_1F1             = ternary "hypergeometric_1F1"             boost_math_hypergeometric_1F1
hypergeometric_1F1_regularized = ternary "hypergeometric_1F1_regularized" boost_math_hypergeometric_1F1_regularized
log_hypergeometric_1F1         = ternary "log_hypergeometric_1F1"         boost_math_log_hypergeometric_1F1

foreign import ccall "boost_math_hypergeometric_pFq" boost_math_hypergeometric_pFq
  :: CInt -> Ptr MPFR -> CInt -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> MPFRPrec -> IO Errno

hypergeometric_pFq :: forall r p . (Rounding r, Precision p) => Vector (Rounded r p) -> Vector (Rounded r p) -> Rounded r p -> Rounded r p
hypergeometric_pFq as bs z =
  unpack $ unsafePerformIO $
  withInRoundedVec as $ \asPtr ->
  withInRoundedVec bs $ \bsPtr ->
  withInRounded z $ \zPtr ->
  withOutRounded' $ \yPtr ->
  boost_math_hypergeometric_pFq num_as asPtr num_bs bsPtr zPtr yPtr prec
  where
    num_as = fromIntegral (V.length as)
    num_bs = fromIntegral (V.length bs)
    prec = fromIntegral (precision (Proxy :: Proxy p))
    unpack (res, info) = defaultCheckInfo "hypergeometric_pFq" res info

hypergeometric_2F1 :: (Rounding r, Precision p) => Rounded r p -> Rounded r p -> Rounded r p -> Rounded r p -> Rounded r p
hypergeometric_2F1 a b c z = hypergeometric_pFq (V.fromList [a,b]) (V.singleton c) z
