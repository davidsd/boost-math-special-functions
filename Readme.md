boost-math-special-functions
============================

A Haskell interface for [boost/math/special_functions](https://www.boost.org/doc/libs/1_76_0/libs/math/doc/html/special.html),
using MPFR for arbitrary precision arithmetic. It relies on the following packages:

 - [rounded](http://hackage.haskell.org/package/rounded), a Haskell library for arbitrary precision arithmetic
 - [GNU MPFR](https://www.mpfr.org/mpfr-current/mpfr.html), the Multiple Precision Floating-Point Reliable Library
 - [boost/math/special_functions](https://www.boost.org/doc/libs/1_71_0/libs/math/doc/html/special.html), a C++ template library implementing several special functions, including gamma functions, elliptic functions, bessel functions, zeta functions, hypergeometric functions, and more.

A majority of the functions from boost/math/special_functions are
supported, but not all. The following are currently **not implemented**
(it would be easy to add them if necessary):

 - number series
 - factorials and binomial coefficients
 - bessel zeros
 - bessel derivatives
 - special polynomials
 - airy zeros
 - jacobi elliptic functions
 - jacobi theta functions
 - lambert_w
 - hypergeometric_pFq_precision

There is currently no support for custom
[Policies](https://www.boost.org/doc/libs/1_71_0/libs/math/doc/html/policy.html).

building
========

To build, you must have the following libraries installed: mpfr, quadmath, boost.

On the Caltech cluster, to get boost it suffices to run 

    module load gcc/9.2.0
    module load boost/1_76_0_gcc-9.2.0

If your system cannot find boost, you might try manually adding it to
extra-include-dirs in your stack.yaml file.
