{-# LANGUAGE DataKinds                #-}
{-# LANGUAGE PolyKinds                #-}
{-# LANGUAGE ScopedTypeVariables      #-}

module Main where

import Numeric.Rounded
import Boost.Math.SpecialFunctions

main :: IO ()
main = do
  print ("tgamma", (x, tgamma x))
  where
    x = 0.123 :: Rounded 'TowardZero 200
