#define BOOST_MATH_DOMAIN_ERROR_POLICY errno_on_error
#include <cerrno>
#include <functional>
#include <vector>
#include "mpfr.h"
#include <boost/multiprecision/mpfr.hpp>
#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/erf.hpp>
#include <boost/math/special_functions/bessel.hpp>
#include <boost/math/special_functions/airy.hpp>
#include <boost/math/special_functions/ellint_1.hpp>
#include <boost/math/special_functions/ellint_2.hpp>
#include <boost/math/special_functions/ellint_3.hpp>
#include <boost/math/special_functions/ellint_d.hpp>
#include <boost/math/special_functions/jacobi_zeta.hpp>
#include <boost/math/special_functions/heuman_lambda.hpp>
#include <boost/math/special_functions/zeta.hpp>
#include <boost/math/special_functions/expint.hpp>
#include <boost/math/special_functions/hypergeometric_1F0.hpp>
#include <boost/math/special_functions/hypergeometric_0F1.hpp>
#include <boost/math/special_functions/hypergeometric_2F0.hpp>
#include <boost/math/special_functions/hypergeometric_1F1.hpp>
#include <boost/math/special_functions/hypergeometric_pFq.hpp>

// Expose functions from boost/math/special_functions to be imported
// to Haskell. Not all functions are currently exposed. Specifically,
// the following are TODO:
//
// number series, factorials and binomial coefficients, bessel zeros,
// bessel derivatives, polynomials, airy zeros, jacobi elliptic
// functions, jacobi theta functions, lambert_w, hypergeometric_pFq_precision

typedef boost::multiprecision::mpfr_float boost_float;

void load_vector(const mpfr_t v_arr, std::vector<boost_float>& v) {
  for (unsigned int i = 0; i < v.size(); ++i) {
    v[i] = boost_float(v_arr + i);
  }
}

int prec_to_digits(mpfr_prec_t prec) {
  return ceil(prec * log(2)/log(10));
}

int unary(std::function<boost_float(boost_float)> go, mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) {
  boost::multiprecision::mpfr_float::default_precision(prec_to_digits(prec));
  boost_float x(x_in);
  boost_float y = go(x);
  mp_rnd_t rnd = mpfr_get_default_rounding_mode();
  mpfr_set(y_out, y.backend().data(), rnd);
  return errno;
}

int binary(std::function<boost_float(boost_float, boost_float)> go, mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec) {
  boost::multiprecision::mpfr_float::default_precision(prec_to_digits(prec));
  boost_float x1(x1_in);
  boost_float x2(x2_in);
  boost_float y = go(x1, x2);
  mp_rnd_t rnd = mpfr_get_default_rounding_mode();
  mpfr_set(y_out, y.backend().data(), rnd);
  return errno;
}

int ternary(std::function<boost_float(boost_float, boost_float, boost_float)> go, mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec) {
  boost::multiprecision::mpfr_float::default_precision(prec_to_digits(prec));
  boost_float x1(x1_in);
  boost_float x2(x2_in);
  boost_float x3(x3_in);
  boost_float y = go(x1, x2, x3);
  mp_rnd_t rnd = mpfr_get_default_rounding_mode();
  mpfr_set(y_out, y.backend().data(), rnd);
  return errno;
}

boost_float myHypergeometric(boost_float z) {
  return boost::math::hypergeometric_pFq({2,3,4}, {5,6,7,8}, z);
}

// gamma.hpp
boost_float boost_math_tgamma_    (boost_float x) { return boost::math::tgamma     (x); }
boost_float boost_math_tgamma1pm1_(boost_float x) { return boost::math::tgamma1pm1 (x); }
boost_float boost_math_lgamma_    (boost_float x) { return boost::math::lgamma     (x); }
boost_float boost_math_digamma_   (boost_float x) { return boost::math::digamma    (x); }
boost_float boost_math_trigamma_  (boost_float x) { return boost::math::trigamma   (x); }
boost_float boost_math_tgamma_ratio_       (boost_float x1, boost_float x2) { return boost::math::tgamma_ratio       (x1, x2); }
boost_float boost_math_gamma_p_            (boost_float x1, boost_float x2) { return boost::math::gamma_p            (x1, x2); }
boost_float boost_math_gamma_q_            (boost_float x1, boost_float x2) { return boost::math::gamma_q            (x1, x2); }
boost_float boost_math_tgamma_lower_       (boost_float x1, boost_float x2) { return boost::math::tgamma_lower       (x1, x2); }
boost_float boost_math_tgamma_inc_         (boost_float x1, boost_float x2) { return boost::math::tgamma             (x1, x2); }
boost_float boost_math_gamma_q_inv_        (boost_float x1, boost_float x2) { return boost::math::gamma_q_inv        (x1, x2); }
boost_float boost_math_gamma_p_inv_        (boost_float x1, boost_float x2) { return boost::math::gamma_p_inv        (x1, x2); }
boost_float boost_math_gamma_q_inva_       (boost_float x1, boost_float x2) { return boost::math::gamma_q_inva       (x1, x2); }
boost_float boost_math_gamma_p_inva_       (boost_float x1, boost_float x2) { return boost::math::gamma_p_inva       (x1, x2); }
boost_float boost_math_gamma_p_derivative_ (boost_float x1, boost_float x2) { return boost::math::gamma_p_derivative (x1, x2); }

// beta.hpp
boost_float boost_math_beta_(boost_float x1, boost_float x2) { return boost::math::beta(x1, x2); }
boost_float boost_math_ibeta_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibeta(x1, x2, x3); }
boost_float boost_math_ibetac_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibetac(x1, x2, x3); }
boost_float boost_math_beta_inc_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::beta(x1, x2, x3); }
boost_float boost_math_betac_inc_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::betac(x1, x2, x3); }
boost_float boost_math_ibeta_inv_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibeta_inv(x1, x2, x3); }
boost_float boost_math_ibetac_inv_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibetac_inv(x1, x2, x3); }
boost_float boost_math_ibeta_inva_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibeta_inva(x1, x2, x3); }
boost_float boost_math_ibetac_inva_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibetac_inva(x1, x2, x3); }
boost_float boost_math_ibeta_invb_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibeta_invb(x1, x2, x3); }
boost_float boost_math_ibetac_invb_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibetac_invb(x1, x2, x3); }
boost_float boost_math_ibeta_derivative_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ibeta_derivative(x1, x2, x3); }

// erf.hpp
boost_float boost_math_erf_      (boost_float x) { return boost::math::erf      (x); }
boost_float boost_math_erfc_     (boost_float x) { return boost::math::erfc     (x); }
boost_float boost_math_erf_inv_  (boost_float x) { return boost::math::erf_inv  (x); }
boost_float boost_math_erfc_inv_ (boost_float x) { return boost::math::erfc_inv (x); }

// bessel.hpp
boost_float boost_math_cyl_bessel_j_ (boost_float x1, boost_float x2) { return boost::math::cyl_bessel_j (x1, x2); }
boost_float boost_math_cyl_neumann_  (boost_float x1, boost_float x2) { return boost::math::cyl_neumann  (x1, x2); }
boost_float boost_math_cyl_bessel_i_ (boost_float x1, boost_float x2) { return boost::math::cyl_bessel_i (x1, x2); }
boost_float boost_math_cyl_bessel_k_ (boost_float x1, boost_float x2) { return boost::math::cyl_bessel_k (x1, x2); }

// airy.hpp: airy_ai, airy_bi, airy_ai_prime, airy_bi_prime
boost_float boost_math_airy_ai_       (boost_float x) { return boost::math::airy_ai       (x); }
boost_float boost_math_airy_bi_       (boost_float x) { return boost::math::airy_bi       (x); }
boost_float boost_math_airy_ai_prime_ (boost_float x) { return boost::math::airy_ai_prime (x); }
boost_float boost_math_airy_bi_prime_ (boost_float x) { return boost::math::airy_bi_prime (x); }

// elliptic integrals

// unary: ellint_1, ellint_2, ellint_d
boost_float boost_math_ellint_1_ (boost_float x) { return boost::math::ellint_1 (x); }
boost_float boost_math_ellint_2_ (boost_float x) { return boost::math::ellint_2 (x); }
boost_float boost_math_ellint_d_ (boost_float x) { return boost::math::ellint_d (x); }

// binary: ellint_1(_inc), ellint_2(_inc), ellint_3, ellint_d(_inc), jacobi_zeta, heuman_lambda
boost_float boost_math_ellint_1_inc_  (boost_float x1, boost_float x2) { return boost::math::ellint_1      (x1, x2); }
boost_float boost_math_ellint_2_inc_  (boost_float x1, boost_float x2) { return boost::math::ellint_2      (x1, x2); }
boost_float boost_math_ellint_3_      (boost_float x1, boost_float x2) { return boost::math::ellint_3      (x1, x2); }
boost_float boost_math_ellint_d_inc_  (boost_float x1, boost_float x2) { return boost::math::ellint_d      (x1, x2); }
boost_float boost_math_jacobi_zeta_   (boost_float x1, boost_float x2) { return boost::math::jacobi_zeta   (x1, x2); }
boost_float boost_math_heuman_lambda_ (boost_float x1, boost_float x2) { return boost::math::heuman_lambda (x1, x2); }

// ternary: ellint_3(_inc)
boost_float boost_math_ellint_3_inc_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::ellint_3(x1, x2, x3); }

// zeta.hpp
boost_float boost_math_zeta_(boost_float x) { return boost::math::zeta(x); }

// expint.hpp
boost_float boost_math_expint_ei_(boost_float x) { return boost::math::expint(x); }

// hypergeometric functions

// binary: hypergeometric_1F0, hypergeometric_0F1
boost_float boost_math_hypergeometric_1F0_       (boost_float x1, boost_float x2) { return boost::math::hypergeometric_1F0       (x1, x2); }
boost_float boost_math_hypergeometric_0F1_       (boost_float x1, boost_float x2) { return boost::math::hypergeometric_0F1       (x1, x2); }

// ternary: hypergeometric_2F0, hypergeometric_1F1, hypergeometric_1F1_regularized, log_hypergeometric_1F1
boost_float boost_math_hypergeometric_2F0_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::hypergeometric_2F0(x1, x2, x3); }
boost_float boost_math_hypergeometric_1F1_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::hypergeometric_1F1(x1, x2, x3); }
boost_float boost_math_hypergeometric_1F1_regularized_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::hypergeometric_1F1_regularized(x1, x2, x3); }
boost_float boost_math_log_hypergeometric_1F1_(boost_float x1, boost_float x2, boost_float x3) { return boost::math::log_hypergeometric_1F1(x1, x2, x3); }

// sequences: hypergeometric_pFq


extern "C" {

  // gamma.hpp
  int boost_math_tgamma    (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_tgamma_,     x_in, y_out, prec); }
  int boost_math_tgamma1pm1(mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_tgamma1pm1_, x_in, y_out, prec); }
  int boost_math_lgamma    (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_lgamma_,     x_in, y_out, prec); }
  int boost_math_digamma   (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_digamma_,    x_in, y_out, prec); }
  int boost_math_trigamma  (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_trigamma_,   x_in, y_out, prec); }

  int boost_math_polygamma(int n, mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) {
    boost::multiprecision::mpfr_float::default_precision(prec_to_digits(prec));
    boost_float x(x_in);
    boost_float y = boost::math::polygamma(n, x);
    mp_rnd_t rnd = mpfr_get_default_rounding_mode();
    mpfr_set(y_out, y.backend().data(), rnd);
    return errno;
  }

  int boost_math_tgamma_ratio       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_tgamma_ratio_       , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_p            (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_p_            , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_q            (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_q_            , x1_in, x2_in, y_out, prec); }
  int boost_math_tgamma_lower       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_tgamma_lower_       , x1_in, x2_in, y_out, prec); }
  int boost_math_tgamma_inc         (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_tgamma_inc_         , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_q_inv        (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_q_inv_        , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_p_inv        (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_p_inv_        , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_q_inva       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_q_inva_       , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_p_inva       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_p_inva_       , x1_in, x2_in, y_out, prec); }
  int boost_math_gamma_p_derivative (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_gamma_p_derivative_ , x1_in, x2_in, y_out, prec); }
  
  // beta.hpp
  int boost_math_beta (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_beta_ , x1_in, x2_in, y_out, prec); }
  int boost_math_ibeta (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibeta_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibetac (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibetac_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_beta_inc (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_beta_inc_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_betac_inc (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_betac_inc_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibeta_inv (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibeta_inv_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibetac_inv (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibetac_inv_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibeta_inva (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibeta_inva_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibetac_inva (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibetac_inva_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibeta_invb (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibeta_invb_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibetac_invb (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibetac_invb_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_ibeta_derivative (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ibeta_derivative_ , x1_in, x2_in, x3_in, y_out, prec); }

  // erf.hpp
  int boost_math_erf      (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_erf_,      x_in, y_out, prec); }
  int boost_math_erfc     (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_erfc_,     x_in, y_out, prec); }
  int boost_math_erf_inv  (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_erf_inv_,  x_in, y_out, prec); }
  int boost_math_erfc_inv (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_erfc_inv_, x_in, y_out, prec); }

  // bessel.hpp
  int boost_math_cyl_bessel_j       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_cyl_bessel_j_       , x1_in, x2_in, y_out, prec); }
  int boost_math_cyl_neumann       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_cyl_neumann_       , x1_in, x2_in, y_out, prec); }
  int boost_math_cyl_bessel_i       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_cyl_bessel_i_       , x1_in, x2_in, y_out, prec); }
  int boost_math_cyl_bessel_k       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_cyl_bessel_k_       , x1_in, x2_in, y_out, prec); }

  // airy.hpp
  int boost_math_airy_ai       (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_airy_ai_,       x_in, y_out, prec); }
  int boost_math_airy_bi       (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_airy_bi_,       x_in, y_out, prec); }
  int boost_math_airy_ai_prime (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_airy_ai_prime_, x_in, y_out, prec); }
  int boost_math_airy_bi_prime (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_airy_bi_prime_, x_in, y_out, prec); }

  // elliptic integrals
  int boost_math_ellint_1      (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_ellint_1_, x_in, y_out, prec); }
  int boost_math_ellint_2      (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_ellint_2_, x_in, y_out, prec); }
  int boost_math_ellint_d      (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_ellint_d_, x_in, y_out, prec); }
  int boost_math_ellint_1_inc  (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_ellint_1_inc_,  x1_in, x2_in, y_out, prec); }
  int boost_math_ellint_2_inc  (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_ellint_2_inc_,  x1_in, x2_in, y_out, prec); }
  int boost_math_ellint_3      (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_ellint_3_,      x1_in, x2_in, y_out, prec); }
  int boost_math_ellint_d_inc  (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_ellint_d_inc_,  x1_in, x2_in, y_out, prec); }
  int boost_math_jacobi_zeta   (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_jacobi_zeta_,   x1_in, x2_in, y_out, prec); }
  int boost_math_heuman_lambda (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_heuman_lambda_, x1_in, x2_in, y_out, prec); }
  int boost_math_ellint_3_inc  (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_ellint_3_inc_, x1_in, x2_in, x3_in, y_out, prec); }

  // zeta.hpp
  int boost_math_zeta (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_zeta_, x_in, y_out, prec); }

  // expint.hpp
  int boost_math_expint_en(unsigned n, mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) {
    boost::multiprecision::mpfr_float::default_precision(prec_to_digits(prec));
    boost_float x(x_in);
    boost_float y = boost::math::expint(n, x);
    mp_rnd_t rnd = mpfr_get_default_rounding_mode();
    mpfr_set(y_out, y.backend().data(), rnd);
    return errno;
  }
  int boost_math_expint_ei (mpfr_t x_in, mpfr_t y_out, mpfr_prec_t prec) { return unary(&boost_math_expint_ei_, x_in, y_out, prec); }

  // hypergeometric functions
  int boost_math_hypergeometric_1F0       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_hypergeometric_1F0_       , x1_in, x2_in, y_out, prec); }
  int boost_math_hypergeometric_0F1       (mpfr_t x1_in, mpfr_t x2_in, mpfr_t y_out, mpfr_prec_t prec)
  { return binary(&boost_math_hypergeometric_0F1_       , x1_in, x2_in, y_out, prec); }
  int boost_math_hypergeometric_2F0 (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_hypergeometric_2F0_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_hypergeometric_1F1 (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_hypergeometric_1F1_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_hypergeometric_1F1_regularized (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_hypergeometric_1F1_regularized_ , x1_in, x2_in, x3_in, y_out, prec); }
  int boost_math_log_hypergeometric_1F1 (mpfr_t x1_in, mpfr_t x2_in, mpfr_t x3_in, mpfr_t y_out, mpfr_prec_t prec)
  { return ternary(&boost_math_log_hypergeometric_1F1_ , x1_in, x2_in, x3_in, y_out, prec); }

  int boost_math_hypergeometric_pFq(int num_as, mpfr_t as_in, int num_bs, mpfr_t bs_in, mpfr_t z_in, mpfr_t y_out, mpfr_prec_t prec) {
    boost::multiprecision::mpfr_float::default_precision(prec_to_digits(prec));
    std::vector<boost_float> as(num_as);
    load_vector(as_in, as);
    std::vector<boost_float> bs(num_bs);
    load_vector(bs_in, bs);
    boost_float z(z_in);
    boost_float y = boost::math::hypergeometric_pFq(as, bs, z);
    mp_rnd_t rnd = mpfr_get_default_rounding_mode();
    mpfr_set(y_out, y.backend().data(), rnd);
    return errno;
  }

}
